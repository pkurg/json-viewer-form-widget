<?php namespace Pkurg\Jsonviewer;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerFormWidgets()
    {
        return [
            'Pkurg\Jsonviewer\FormWidgets\JSONViewerWidget' => 'jsonviewer_widget',

        ];
    }

    public $properties;

    public function boot()
    {

        $this->properties = [
            'collapsed' => [
                'title' => 'All nodes are collapsed',
                'type' => 'checkbox',
                'default' => true,
            ],
            'root_collapsable' => [
                'title' => 'Allow root element to be collasped',
                'type' => 'checkbox',
                'default' => true,

            ],
            'with_quotes' => [
                'title' => 'All JSON keys are surrounded with double quotation marks',
                'type' => 'dropdown',
                'type' => 'checkbox',
                'default' => true,

            ],
            'with_links' => [
                'title' => 'All values that are valid links will be clickable',
                'type' => 'checkbox',
                'default' => true,

            ],
        ];

        \Event::listen('pages.builder.registerControls', function ($controlLibrary) {
            $controlLibrary->registerControl(
                'jsonviewer_widget',
                'JSON Viewer',
                'Easily displaying JSON objects',
                \RainLab\Builder\Classes\ControlLibrary::GROUP_WIDGETS,
                'oc-icon-database',
                $controlLibrary->getStandardProperties([], $this->properties),
                'Pkurg\Jsonviewer\Classes\ControlDesignTimeProvider'
            );
        });
    }

}
