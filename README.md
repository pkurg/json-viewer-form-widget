
JSON Viewer widget for easily displaying JSON objects by transforming them into HTML.


**The supported options are:**

 - **collapsed** (boolean, default: false): all nodes are collapsed at html generation.
 - **root_collapsable** (boolean, default: true): allow root element to be collasped.
 - **with_quotes** (boolean, default: false): all JSON keys are surrounded with double quotation marks ({"foobar": 1} instead of {foobar: 1}).
 - **with_links** (boolean, default: true): all values that are valid links will be clickable, if false they will only be strings.



**Example:**

```
    json_field:
        label: 'JSON Viewer'
        type: jsonviewer_widget
        collapsed: true
        root_collapsable: true
        with_quotes: false
        with_links: true
        
```

![](https://i.ibb.co/445W8wk/previev.png)


**Rainlab.Builder plugin supported**


![](https://i.ibb.co/jwQgPMf/addcontrol.png)
![](https://i.ibb.co/WtzMtgW/properties.png)