<?php return [
    'plugin' => [
        'name' => 'JSONViewer',
        'description' => 'Widget for easily displaying JSON objects by transforming them into HTML.'
    ]
];