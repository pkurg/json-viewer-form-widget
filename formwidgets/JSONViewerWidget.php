<?php namespace Pkurg\Jsonviewer\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * JSONViewerWidget Form Widget
 */
class JSONViewerWidget extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'jsonviewer_widget';

    //{collapsed: true, withQuotes: true, withLinks: false});

    // properties

    public $collapsed = false;

    public $with_quotes = true;

    public $with_links = false;

    public $root_collapsable = true;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig([
            'collapsed',
            'root_collapsable',
            'with_quotes',
            'with_links',

        ]);
    }

    public function isJson($string)
    {

        if (is_array($string)) {
            return false;
        }
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('jsonviewerwidget');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();

        $value = $this->getLoadValue();

        if (!$this->isJson($value)) {

            if (is_array($value)) {

                $value = json_encode($value, true);

            } else {

                $value = '{"info": "No valid JSON string"}';
            }

        }
        

        $this->vars['value'] = $value;

        $this->vars['model'] = $this->model;

        $this->vars['collapsed'] = $this->collapsed;
        $this->vars['root_collapsable'] = $this->root_collapsable;
        $this->vars['with_quotes'] = $this->with_quotes;
        $this->vars['with_links'] = $this->with_links;

    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/jquery.json-viewer.css', 'Pkurg.Jsonviewer');
        $this->addJs('js/jquery.json-viewer.js', 'Pkurg.Jsonviewer');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return $value;
    }
}
